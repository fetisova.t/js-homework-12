/**
 * Created on 04.07.2019.
 */
//Опишите своими словами разницу между функциями setTimeout() и setInterval().
    //setTimeout - откладывает выполнение указанной в ней функции на определенное количество времени 1 раз. setInterval - это цикличное повторение фукнции, через определенные промежутки времени, пока не наступает условие выхода из цикла.
//Что произойдет, если в функцию setTimeout() передать нулевую задержку? Сработает ли она мгновенно, и почему?
    //любое значение таймаута в пределах от 0 до 4мс даст одинаковый результат, так как у каждого браузера сущетвует собственная минимальная задержка, то есть даже если установлен 0, таймаут может все равно сработать только по прошествии 4мс, не мгновенно
//Почему важно не забывать вызывать функцию clearInterval(), когда ранее созданный цикл запуска вам уже не нужен?
    //Так как эти функции продолжают работать, даже если вкладка со страницей неактивна

const images = document.querySelectorAll('.image-to-show');
let imgN=1;
let imgLength = images.length;
let interval;

function showImg(n) {
    if (n > imgLength) imgN = 1;
    else if (n < 1) imgN = imgLength;
    for (let i=0; i<imgLength; i++){
        images[i].style.display = "none";
    }
    images[imgN-1].style.display = "block";
}
function nextImg(n) {
    showImg(imgN += n);
}
function setImgInterval(){
    interval = setInterval(function () {
        nextImg(1);
    },10000);
}
setImgInterval();

let buttonStop = document.createElement("button"),
    buttonPlay = document.createElement("button");
imagesWrapper = document.getElementsByClassName("images-wrapper")[0];
buttonStop.innerText = "Прекратить";
buttonPlay.innerText = "Возобновить показ";

document.body.before(buttonStop, imagesWrapper);
document.body.before(buttonPlay, imagesWrapper);


buttonStop.addEventListener('click',()=>{
    clearInterval(interval);
});
buttonPlay.addEventListener('click',()=>{
    setImgInterval();
});

//неудачные прошлые попытки

// images.forEach((elem)=>{
//     if(elem.classList.contains('active')){
//         // continue;
//     }else{
//         setTimeout(()=>{
//             if(elem.previousElementSibling){
//                 if(elem.previousElementSibling.classList.contains('active')){
//                     elem.previousElementSibling.classList.remove('active');
//                 }
//             }
//             elem.classList.add('active');
//         }, time+=2000);
//
//     }
//
//
//
// });

// images.forEach((elem)=>{
//
//         setTimeout(()=>{
//             if(elem.previousElementSibling){
//                 if(elem.previousElementSibling.classList.contains('active')){
//                     elem.previousElementSibling.classList.remove('active');
//                 }
//             }
//             elem.classList.add('active');
//         }, time+=2000);
//
//
//
//
//
// });



// for (let i = 0; i<imgLength; i++){
//
//     if(images[i].nextElementSibling){
//         if(images[i].classList.contains('active')){
//             continue;
//         }else{
//             setTimeout(()=>{
//                 if(images[i].previousElementSibling){
//                     if(images[i].previousElementSibling.classList.contains('active')){
//                         images[i].previousElementSibling.classList.remove('active');
//                     }
//                 }
//                 images[i].classList.add('active');
//             }, time+=2000);
//
//         }
//     }else{
//         images[i].previousElementSibling.classList.remove('active');
//         images[i].classList.add('active');
//         // i=0;
//     }
//
// }
